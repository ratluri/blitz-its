const nodemailer = require('nodemailer');
const express = require('express');
const sgTransport = require('nodemailer-sendgrid-transport');
const multer = require('multer');
const fetch = require('node-fetch');

const { PORT, RECIPIENT_EMAIL, SENDGRID_API_KEY, RECAPTCHA_SECRET_KEY } = require('./constants');

const upload = multer({ dest: 'uploads/' });
const app = express();

app.use((req, res, next) => {
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

app.use(express.static('_site'));

var options = {
  auth: { api_key: SENDGRID_API_KEY },
};

var client = nodemailer.createTransport(sgTransport(options));

var template = {
  to: RECIPIENT_EMAIL,
};

app.post('/apply', upload.any(), function (req, res) {
  const { first_name, last_name, email, phone, message, subject, linkedin } = req.body;

  client.sendMail(
    Object.assign({}, template, {
      from: email,
      subject: 'Apply',
      text: `${first_name} / ${last_name} / ${email} / ${phone} / ${message} / ${subject} / ${linkedin}`,
      attachments: [{ filename: 'file', content: req.files[0] }]
    }), (err, info) => console.log(err, info)
  );

  return res.send({});
});

app.post('/contact', upload.any(), function (req, res) {
  const { first_name, last_name, email, phone, message } = req.body;

  fetch([
    'https://www.google.com/recaptcha/api/siteverify?',
    `secret=${RECAPTCHA_SECRET_KEY}&`,
    `response=${req.body['g-recaptcha-response']}`,
  ].join(''), {
    method: 'POST',
  }).then(r => r.json()).then((r) => {
    if (!r.success) return;

    return client.sendMail(
      Object.assign({}, template, {
        from: email,
        subject: 'Contact',
        text: `${first_name} / ${last_name} / ${email} / ${phone} / ${message}`,
      }), (err, info) => console.log(err, info)
    );
  });

  return res.send({});
});

app.listen(PORT, '0.0.0.0', (err) => {
  if (err) console.log(err);
  console.info('==> 🌎 Listening on port %s. Open up http://0.0.0.0:%s/ in your browser.', PORT, PORT);
});

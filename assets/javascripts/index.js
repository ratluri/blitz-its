var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
var b = document.querySelector('body');
var contactsRecaptcha = false;
var applyRecaptcha = false;

function showToast(selector) {
  var el = document.querySelector(selector);
  el.classList.add('toast--active');
  setTimeout(function() { el.classList.remove('toast--active') }, 3000);
}

var init = function(sliderSelector, buttonsContainerSelector, perPage, loop) {
  if (!document.querySelector(sliderSelector)) return;
  perPage = perPage || 1;
  var slider = new Siema({
    loop: loop,
    perPage: perPage,
    selector: sliderSelector,
    onChange: function() { changeActive(slider.currentSlide); },
  });

  var changeActive = function(j) {
    document.querySelector(buttonsContainerSelector + ' .buttons-circle__item--active').classList.remove('buttons-circle__item--active');
    document.querySelectorAll(buttonsContainerSelector + ' .buttons-circle__item')[j].classList.add('buttons-circle__item--active');
  }

  for (var i = 0; i < slider.innerElements.length - (perPage - 1); i++) {
    var btn = document.createElement('button');
    btn.classList.add('buttons-circle__item');
    if (i === 0) btn.classList.add('buttons-circle__item--active');

    btn.addEventListener('click', (function(j) {
      return function() {
        changeActive(j);
        slider.goTo(j);
      }
    })(i));

    document.querySelector(buttonsContainerSelector).appendChild(btn);
  }

  return slider;
}

// sliders
var heroSlider = init('.hero__slider', '.hero__slider-buttons--circles', 1, true);
var heroNextButton = document.querySelector('#hero-next-button');
var heroPrevButton = document.querySelector('#hero-prev-button');

if (heroNextButton && heroPrevButton) {
  heroNextButton.addEventListener('click', function() { heroSlider.next() });
  heroPrevButton.addEventListener('click', function() { heroSlider.prev() });
}

if (w <= 1024) init('.services__slider', '.services__slider-buttons', w > 767 ? 2 : 1);
init('.projects__slider', '.projects__slider-buttons', w > 1024 ? 3 : w > 767 ? 2 : 1);
if (w <= 1024) init('.technologies__slider', '.technologies__slider-buttons', w > 767 ? 2 : 1);
init('.feedback__slider', '.feedback__slider-buttons', w > 1024 ? 2 : 1);
init('.clients__slider', '.clients__slider-buttons', w > 1024 ? 3 : w > 767 ? 2 : 1);
init('.team__slider', '.team__slider-buttons', w > 1024 ? 3 : w > 767 ? 2 : 1);

// contacts form
var contactsForm = document.querySelector('#contacts');

if (contactsForm) {
  var firstNameInput = document.querySelector('#contacts [name=first_name]');
  var lastNameInput = document.querySelector('#contacts [name=last_name]');
  var emailInput = document.querySelector('#contacts [name=email]');
  var phoneInput = document.querySelector('#contacts [name=phone]');
  var messageInput = document.querySelector('#contacts [name=message]');
  var overlay = document.querySelector('#contacts-overlay');

  contactsForm.addEventListener('submit', function(e) {
    e.preventDefault();
    if (!firstNameInput.value || !lastNameInput.value || !emailInput.value || !phoneInput.value || !messageInput.value || !contactsRecaptcha) {
      return showToast('#contacts .toast');
    }
    var formData = new FormData(contactsForm);

    return fetch('/contact', {
      method: 'POST',
      body: formData
    }).then(
      function(resp) { return resp.json() }
    ).then(function(resp) {
      overlay.classList.add('contacts-form__success-overlay--active');
      setTimeout(function() {
        overlay.classList.remove('contacts-form__success-overlay--active');
      }, 2500);
      firstNameInput.value = '';
      lastNameInput.value = '';
      emailInput.value = '';
      phoneInput.value = '';
      messageInput.value = '';
    }).catch(
      function(err) { return console.log(err) }
    );
  });
}

// print
var printButton = document.querySelector('#print-button');

if (printButton) {
  printButton.addEventListener('click', function() { window.print(); })
}

// apply form
var applyForm = document.querySelector('#apply');
var fileInput = document.querySelector('#apply [name=file-to-upload]');
var filename = document.querySelector('#fileinput-filename');

if (applyForm) {
  var applyButton = document.querySelector('#apply-button');
  var applyCloseButton = document.querySelector('#apply-close-button');
  var applyDoneCloseButton = document.querySelector('#apply-done-close-button');
  var applyModal = document.querySelector('#apply-modal');
  var applyModalDone = document.querySelector('#apply-modal-done');
  var applyModalContent= document.querySelector('#apply-modal-content');
  var applySubmitButton = document.querySelector('#apply-submit-button');

  var firstNameInput = document.querySelector('#apply [name=first_name]');
  var lastNameInput = document.querySelector('#apply [name=last_name]');
  var emailInput = document.querySelector('#apply [name=email]');
  var phoneInput = document.querySelector('#apply [name=phone]');
  var subjectInput = document.querySelector('#apply [name=subject]');
  var messageInput = document.querySelector('#apply [name=message]');
  var linkedinInput = document.querySelector('#apply [name=linkedin]');

  var submit = function() {
    if (!fileInput.value || !firstNameInput.value || !lastNameInput.value || !emailInput.value || !phoneInput.value || !applyRecaptcha) {
      return showToast('#apply .toast');
    }

    var formData = new FormData(applyForm);
    var file = fileInput.files[0];
    formData.append('files', file, file.name);

    return fetch('/apply', {
      method: 'POST',
      body: formData
    }).then(
      function(resp) { return resp.json() }
    ).then(function() {
      // successfull submit
      applyModalDone.classList.add('apply-modal--active');
      applyModal.classList.remove('apply-modal--active');

      firstNameInput.value = '';
      lastNameInput.value = '';
      emailInput.value = '';
      phoneInput.value = '';
      fileInput.value = '';
      subjectInput.value = '';
      messageInput.value = '';
      linkedinInput.value = '';
      filename.innerText = '';
    }).catch(
      // error
      function(err) { return console.log(err) }
    );
  };

  applySubmitButton.addEventListener('click', submit);

  applyButton.addEventListener('click', function() {
    b.classList.add('body--modal-opened');
    applyModal.classList.add('apply-modal--active');
  });

  applyCloseButton.addEventListener('click', function() {
    b.classList.remove('body--modal-opened');
    applyModal.classList.remove('apply-modal--active');
  });

  applyDoneCloseButton.addEventListener('click', function() {
    b.classList.remove('body--modal-opened');
    applyModalDone.classList.remove('apply-modal--active');
  });

  applyModalContent.addEventListener('click', function(e) {
    e.stopPropagation();
  });

  applyModal.addEventListener('click', function(e) {
    b.classList.remove('body--modal-opened');
    applyModal.classList.remove('apply-modal--active');
  });
}

// navigation
var navigationButton = document.querySelector('#navigation-button');
var navigation = document.querySelector('#navigation');

navigation.addEventListener('click', function(e) {
  e.stopPropagation();
});

navigationButton.addEventListener('click', function(e) {
  e.stopPropagation();
  b.classList.add('body--nav-opened');
  navigation.classList.add('navigation--active');
});

b.addEventListener('click', function(e) {
  if (b.classList.contains('body--nav-opened')) {
    b.classList.remove('body--nav-opened');
    navigation.classList.remove('navigation--active');
  }
});

if (filename && fileInput) {
  fileInput.addEventListener('change', function() {
    filename.innerText = fileInput.files[0].name;
  });
}

function contactsRecaptchaSuccessCallback() { contactsRecaptcha = true; }
function contactsRecaptchaExpiredCallback() { contactsRecaptcha = false; }
function applyRecaptchaSuccessCallback() { applyRecaptcha = true; }
function applyRecaptchaExpiredCallback() { applyRecaptcha = false; }

window.contactsRecaptchaSuccessCallback = contactsRecaptchaSuccessCallback;
window.contactsRecaptchaExpiredCallback = contactsRecaptchaExpiredCallback;
window.applyRecaptchaSuccessCallback = applyRecaptchaSuccessCallback;
window.applyRecaptchaExpiredCallback = applyRecaptchaExpiredCallback;

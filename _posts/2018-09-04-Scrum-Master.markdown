---
layout: post
date: 2018-09-04 12:40:21 +0300

title: "Scrum Master"
location: "Baltimore, MD"
industry: "Software"
compensation: "No"
telecommute: "No"
type: "Part-time"
work_authorization: "Contract"
---

Basic Qualifications:

* Bachelor's degree and 14 years of relative experience, Master's degree and 12 years of relative experience, or 18 years of relative experience in an IT field in lieu of a degree.
* Minimum of 3 years' of experience working as part of the Scrum team (as Scrum Master, Product Owner, Team member, or a combination thereof)
* Minimum of 9 years of experience working in an IT related field
* Experience teaching others how to become Agile
* Certification: Scrum Alliance Certified Scrum Master (CSM)
* Must be able to obtain and maintain a Public Trust clearance.
* Must be a US citizen or green card holder.

Preferred Qualifications:

* Experience with and understanding of doing business with the federal government.
* Ability to understand technical legacy systems and associated component functionalities.
* Experience with enterprise architecture, application planning, and deployment.
* Experience leading efforts to prepare life cycle documents, including Use Cases, Requirements System Design
* Alternatives, System Development Plans, Detailed Design Specifications, and Implementation Plans.

Preferred Certifications:

* Certification as Agile Coach
* Agile Certified Practitioner
* Certification as a Product Owner (CPO) ,
* SAFe Program Consultant (SPC)
* Project Management Professional (PMP)®

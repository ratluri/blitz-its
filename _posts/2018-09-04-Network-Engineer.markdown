---
layout: post
date: 2018-09-04 12:40:21 +0300

title: "Network Engineer"
location: "Windsor Mill, MD"
industry: "Software"
compensation: "No"
telecommute: "No"
type: "Part-time"
work_authorization: "Contract"
---

Basic Qualifications:

* Bachelors degree and 10 years of IT related work experience, Masters degree and 8 years of IT related experience or Doctorate and 5 years of IT related experience
* Must hold one of the following active certifications:
    * Palo Alto Networks Certified Network Security Engineer (PCNSE) \
    * Palo Alto Accredited Configuration Engineer (ACE) certification
* Minimum of 8 years of experience in engineering complex network systems.
* Minimum of 2 years of experience with network security control systems. For example: firewalls, anti-virus, intrusion detection, wireless, proxy server, VPN, TACACS, RADIUS, ACLs, IPSEC, 802.1x, audits, log files, etc.
* Strong understanding of Cisco switches/routers and principle LAN/WAN technologies (Ethernet, IP routing, Multicast, DHCP, etc.).
* Demonstrate strong oral and written communication skills, with the ability to communicate technical topics to management and non-technical audiences, as well as interface with the customer on a daily basis
* Must be US Citizen or Lawful Permanent Resident
* Must be able to obtain a Public Trust clearance

Preferred Qualifications:

* AIX Administration certification
* Network engineering industry certification (Network+, CCNA, CCNP, CISSP)
* Knowledge of IP routing protocols and implementations

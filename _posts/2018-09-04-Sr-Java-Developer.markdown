---
layout: post
date: 2018-09-04 12:40:21 +0300

title: "Sr. Java Developer"
location: "Baltimore, MD"
industry: "Software"
compensation: "No"
telecommute: "No"
type: "Part-time"
work_authorization: "Contract"
---

Required Skills:

* Candidate must have an advanced level of expertise with the following:
* Hands-on experience with Front End Technologies including React JS, Stencil JS, and ECMAScript 6.
* Strong experience with Middleware/Backend Technologies including Spring Boot (Spring Data, Spring Security OAuth using JWT, Spring HATEOS, Spring Web Services), Apache Camel and Camel Components, Apache CXF, Redis, Rules Engine (e.g. – Drools), PostgreSQL, and Swagger.
* Experience with Java 8
* Experience with Cloud deployment, Dockers, and OpenShift.
* Strong Experience working in Agile environment.
* Communication skills to present ideas and concepts effectively; strong and proven problem solving.

Desired Skills:

* Experience with JEE technologies, Struts 1.3x and Struts 2, DBC, JPA, Hibernate, SOAP/RESTful, XML, PL/SQL, Oracle, DB2, SonarQube, Git, Tomcat, WebSphere and VersionOne in an Agile software development environment.
* Experience with Web Services Technology (XML, JAXP, JAXB, JAX-WS, AXIS, JERSEY, JSON, CXF, REST and SOAP) and Security (LTPA, JWT, OAUTH2).
* Strong XML processing experience such as XSD, XPath, XSL, XSLT, etc.
* Experience with Object Oriented Design (OOD), Object-oriented programming (OOP) and development, data structures and design patterns.
* Experience with Agile framework and ability to apply best practices using industry standards throughout the software development phase.
* Experience with front-end technologies such as JavaScript, TypeScript, JQuery, Ajax, HTML5, CSS3, and Bootstrap.
* Hands-on experience with various frameworks such as AngularJS, jQuery, and node.js.
* Experience coding "responsive " web pages, with cross-browser limitations and standard-compliant.
* Experience with RDBMS and database design and troubleshooting (Oracle, PL/SQL, DB2, SQL Server, PostgreSQL)
* Experience with Automated unit test (i.e. JUnit, Mocking frameworks), Test Driven Development (TDD), and Behavior-driven development (BDD) (Cucumber).
* Experience with writing Java Batch applications with spring batch or other batch frameworks.
* Knowledge of web application security vulnerabilities and how to address them.
* Experience with DevOps, full automated deployment (Continuous Integration/Continuous Delivery) and test automation, working knowledge of Jenkins, Maven and Shell Scripting.
* Experience with building Spring based, cloud native, 12 factor applications.
* Hands-on Design and development experience on Microservices and Restful API.
* Experience with designing, architecting, and presenting technical Cloud-based solutions throughout the life cycle, from early capture through proposal and delivery.
* Experience with complex programming, program debugging, data analysis, problem analysis and resolution issues within OO application systems.
* Strong communication and documentation skills.
* Self-starter, highly motivated individual, a team player who adapts to a dynamic work environment, and the ability to mentor others.
* Experience with version control software such as MKS and GIT.
* Able to multi-task and work in a dynamic, fast-pace environment.
* Ability to investigate/research issues, determines impact, and provide solutions.
* Experience with the implementation of Section 508 standards for Web Applications.
* Exposure to writing Technical White Papers and systems design documents.
* Prior experience with federal or state governments IT projects.

Education:
Master's Degree is Required

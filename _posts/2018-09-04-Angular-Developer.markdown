---
layout: post
date: 2018-09-04 12:40:21 +0300

title: "Angular Developer"
location: "Richmond, VA"
industry: "Software"
compensation: "No"
telecommute: "No"
type: "Part-time"
work_authorization: "Contract"
---

* Proficiency in Mobilink data synchronization and communication technology
* Knowledge and experience setting up mobilink consolidated database, remote database and the synchronization server.
* Knowledge and experience programming database synchronization rules using mobilink platform.
* Portfolio or samples of past work on Mobilink synchronization and distributed database projects
* Familiarity with the general database synchronization and distributed database technologies.
* Seven plus years in a front end development role
* Strong knowledge and experience with Angular 4 or higher developing complex web applications: Angular 6 is preferred
* Experience developing responsive web applications using HTML5, JavaScript ES6, CSS3, Bootstrap v3 and/or v4, Electron framework and consuming web services.
* Knowledge of unit testing techniques
* Willingness to mentor and advise in-house team and provide knowledge transfer
* Strong communication skills to effectively collaborate with designers, developers, functional architects and other relevant staff members or clients
* Ability to interpret and implement application and feature designs from prototypes and sketches

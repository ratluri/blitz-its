---
layout: post
date: 2018-09-04 12:40:21 +0300

title: "Network Admin"
location: "Windsor Mill, MD"
industry: "Software"
compensation: "No"
telecommute: "No"
type: "Part-time"
work_authorization: "Contract"
---

Basic Qualifications:

* Bachelor's degree and 6 years of relative experience, Master's degree and 4 years of relative experience, or 10 years of relative experience in an IT field in lieu of a degree.
* Minimum of 4 years of networking equipment such as routers and switches
* Minimum of 2 years of RUBY, PERL, or Python scripting experience on a Red Hat Enterprise Linux or UNIX platform
* Minimum of 1 year of Cisco PRIME or Cisco WORKS of experience
* Minimum of 1 year of commonly used automation tools such as Ansible, Puppet
* Must be US Citizen or Lawful Permanent Resident
* Must be able to obtain a Public Trust clearance

Preferred Qualifications:

* Experience with version control (CVS, Subversion, Git, etc.)
* Strong understanding of and compliance with IT development process (SDLC).
* Experience with commonly used tools such as JIRA, Confluence, BitBucket
* Strong requirements planning and project implementation skills and be able to work with internal technical staff and outside contractors to deliver results.
* JavaScript, CSS, XML, and HTML 5 experience a plus
* Certification in one or more of the following:
* Red Hat Certified System Administrator (RHCSA)
* Cisco Certified Network Associate (CCNA)

### Setting up Jekyll

The thing is used for building the app together is called Jekyll - https://jekyllrb.com/. Jekyll works in a ruby environment (your mac should have it built-in, actually), speaking more specifically, Jekyll is a ruby `gem` (~package). The best way to manage ruby gems on your mac is `bundler`. So, at first, you need to install the `bundler` itself:

```
$ gem install bundler
```

After this completes, you can `cd` into the repo and then run gems installation:

```
$ bundle install
```

This will install Jekyll gem itself and all the dependencies. After this completes you can either

```
$ bundle exec jekyll serve
```

to start the local dev server on `localhost:4000` or

```
$ bundle exec jekyll build
```

to build the app into `./_site` folder.


### Managing non-jobs content

Main rule is: do not touch anything in a `./_site` folder manually - that's a destination folder of the app build.

`./index.html` - source file of the landing page
`./services.html` - source file of the services page
`./about.html` - source file of the about page
`./benefits.html` - source file of the benefits page
`./contacts.html` - source file of the contacts page
`./careers.html` - source file of the list of openings

Feel free to change all the text (and html, if you want) in those files, except the special ruby tags - `{% ... %}` and `{{ ... }}`. Those are the conditionals and loops mostly. Style files are located in `./_sass` folder. Source files for images are located in `./assets/images`. Source files for scripting - `./assets/javascripts`. Data (in a json format) for most of the slideshows on all the pages are located in `./_data` folder. That's it.


### Managing jobs/openings

All the openings are located in `./_posts` folder. To add a new opening just add a `.markdown` file to the `./_posts` folder. You need to be familiar with a markdown syntax - that's something similar to what jira/confluence has, actually.

Each opening file contains a Front Matter block:

```
---
layout: post
date: 2018-09-04 12:40:21 +0300

title: "Some job title Some job title Some job title Some job title Some job title"
location: "Some location"
industry: "Some industry"
compensation: "No"
telecommute: "No"
type: "Part-time"
work_authorization: "Contract"
---
```

(See https://jekyllrb.com/docs/front-matter/ for details. This is a metadata needed to show the opening properly. Please leave the `layout: post` untouched for all the openings. Feel free to change any othere fields.)

```
And the actual opening description in a markdown format.
```

After new openings were added, you need to run the

```
$ bundle exec jekyll build
```

to build the app to include all the new openings to the list. In case you are using the `$ bundle exec jekyll serve` command build is performed automatically each time you change something in a source files / add new openings / remove old openings.


### Deploy

* Copy `./_site`, `index.js`, `constants.js`, `package.json` to the server;
* Run `npm i` to install all the needed dependencies (or do not copy `package.json` and install all the dependencies by hands);
* Start the node server: `node index.js`. Server starts on the port 8080, so you need to map port 8080 to the default (?) 80 port of the machine.

### Deployment issues

* Check all the ports for node.js running with the port 80 and kill node processes
  [root@ip-172-31-69-73 html]# lsof -i tcp:80
  COMMAND  PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
  node    5711 root   10u  IPv4  41117      0t0  TCP *:http (LISTEN)
  node    5711 root   11u  IPv4  41121      0t0  TCP ip-172-31-69-73.ec2.internal:http->ip-172-31-74-123.ec2.internal:9976 (ESTABLISHED)
  node    5711 root   12u  IPv4  41123      0t0  TCP ip-172-31-69-73.ec2.internal:http->ip-172-31-12-83.ec2.internal:42318 (ESTABLISHED)
  
* Restart the node process using forever to have persistent connection
  [root@ip-172-31-69-73 html]# forever start index.js
  warn:    --minUptime not set. Defaulting to: 1000ms
  warn:    --spinSleepTime not set. Your script will exit if it does not stay up for at least 1000ms
  info:    Forever processing file: index.js
 
  
